﻿using Domain.Aggregates.Entities;
using Domain.Shared.Entities;
using Domain.Shared.Interface;

namespace Domain.Aggregates
{
    public class CategoryAggregate : Entity<Guid>, IAggregateRoot
    {
        private List<Image> _listImage;
        public string Name { get; private set; }
        public string Description { get; private set; }
        public Guid ParentCateId { get; private set; }
        public int Level { get; private set; }
        public IReadOnlyCollection<Image> Gallery => _listImage.AsReadOnly();

        private CategoryAggregate(Guid Id) : base(Id)
        {
            this._listImage = new List<Image>();
        }

        public CategoryAggregate(string name) : this(Guid.NewGuid())
        {
            this.Name = name;
        }

        public CategoryAggregate(Guid id, string name) : this(id)
        {
            this.Name = name;
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public void SetDescription(string description)
        {
            this.Description = description;
        }

        public void SetCategoryInfo(string name, string description, int level, Guid parentCateId)
        {
            this.Name = name;
            this.Description = description;
            this.ParentCateId = parentCateId;
            this.Level = level;
        }

        public void SetCategoryInfo(string description, int level, Guid parentCateId)
        {
            this.Description = description;
            this.ParentCateId = parentCateId;
            this.Level = level;
        }

        public void SetParentCategory(Guid parentCateId)
        {
            this.ParentCateId = parentCateId;
        }

        public void SetLevel(int level)
        {
            this.Level = level;
        }

        public void SetGallery(IEnumerable<string> paths)
        {
            foreach (var path in paths)
            {
                _listImage.Add(new Image(Guid.NewGuid(), path));
            }
        }

        public void SetGallery(IEnumerable<Image> images)
        {
            _listImage.AddRange(images);
        }

    }
}
