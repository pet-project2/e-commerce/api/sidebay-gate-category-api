﻿using Domain.Shared.Entities;

namespace Domain.Aggregates.Entities
{
    public class Image : Entity<Guid>
    {
        public string Name { get; set; }
        public string Path { get; private set; }
        public bool IsMainAvatar { get; private set; }
        private Image(Guid Id) : base(Id)
        {

        }

        public Image(string path) : this(Guid.NewGuid())
        {
            this.Path = path;
        }

        public Image(Guid id, string path) : this(id)
        {
            this.Path = path;
        }

        public void SetMainAvatar()
        {
            this.IsMainAvatar = true;
        }
    }
}
