﻿namespace Domain.Shared.Models
{
    public interface IAuditable
    {
        DateTime CreatedDateTime { get; }

        DateTime LastModifiedDateTime { get; }
    }


    public abstract class BaseModel<T> : IAuditable
    {
        public T Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastModifiedDateTime { get; set; }

        public void SetCreatedDateTime()
        {
            this.CreatedDateTime = DateTime.UtcNow;
        }

        public void SetLastMofifiedDateTime()
        {
            this.LastModifiedDateTime = DateTime.UtcNow;
        }
    }
}
