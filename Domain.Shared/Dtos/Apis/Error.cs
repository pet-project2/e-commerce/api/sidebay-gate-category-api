﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Domain.Shared.Dtos.Apis
{
    public enum ErrorType
    {
        [Description("Model state is invalid")]
        MODEL_INVALID = 0,

        [Description("Internal Server Error, Something is happened")]
        INTERNAL_SERVER_ERROR = 1,

        [Description("Data is existed")]
        EXISTED = 2,

        [Description("Data created failed")]
        CREATED = 3
    }
    public class Error<T>
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ErrorType Type { get; private set; }
        public T Reason { get; private set; }

        public Error(ErrorType errorType, T reason)
        {
            this.Type = errorType;
            this.Reason = reason;
        }
    }
}
