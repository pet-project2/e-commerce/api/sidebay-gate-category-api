﻿using System.Text.Json;

namespace Domain.Shared.Dtos.Apis
{
    public class ApiErrorResponse<T> : ApiBaseResponse
    {
        public Error<T> Error { get; private set; }
        public ApiErrorResponse() : base(false)
        {

        }

        private ApiErrorResponse(string message, int statusCode) :
            base(false, message, statusCode)
        {

        }

        public ApiErrorResponse(string message, int statusCodes, ErrorType errorType, T reason) :
            this(message, statusCodes)
        {
            this.Error = new Error<T>(errorType, reason);
        }


        public override string ToString()
        {
            return JsonSerializer.Serialize(this, new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            });
        }
    }
}
