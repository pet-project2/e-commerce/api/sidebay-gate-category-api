﻿namespace Domain.Shared.Dtos.Resuls
{
    public interface IResultData<T> : IResult
    {
        T Data { get; }
    }
}
