﻿namespace Domain.Shared.Dtos.Resuls
{
    public interface IResult
    {
        public bool IsSuccess { get; }
        public string Message { get; }
    }
}
