﻿namespace Domain.Shared.Dtos.Resuls
{
    public class ListDto<T>
    {
        public IEnumerable<T> Data { get; set; }
        public Paging Paging { get; set; }
    }
}
