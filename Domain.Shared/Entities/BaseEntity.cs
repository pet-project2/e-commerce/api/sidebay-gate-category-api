﻿using MediatR;

namespace Domain.Shared.Entities
{
    public abstract class BaseEntity<TId>
    {
        private List<INotification> _domainEvents;
        public TId Id { get; private set; }
        public bool IsDeleted { get; private set; }
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents.AsReadOnly();

        public BaseEntity(TId Id)
        {
            this.Id = Id;
            this._domainEvents = new List<INotification>();
        }

        public void AddDomainEvent(INotification @event)
        {
            this._domainEvents?.Add(@event);
        }

        public void RemoveDomainEvent(INotification @event)
        {
            this._domainEvents?.Remove(@event);
        }

        public void SetDelete()
        {
            this.IsDeleted = true;
        }

        public void ClearDomainEvents()
        {
            this._domainEvents?.Clear();
        }
    }
}
