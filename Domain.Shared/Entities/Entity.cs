﻿namespace Domain.Shared.Entities
{
    public class Entity<TId> : BaseEntity<TId>
    {
        private int? _requestedHashCode;
        public Entity(TId Id) : base(Id)
        {
            if (this.IsTransient() || object.Equals(Id, null))
            {
                throw new EntityException("The Ids cannot be null or the default value.");
            }
        }

        public bool IsTransient()
        {
            return object.Equals(Id, default(TId));
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Entity<TId>))
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            if (this.GetType() != obj.GetType())
                return false;

            Entity<TId> item = obj as Entity<TId>;

            if (item.IsTransient() || this.IsTransient())
                return false;

            return object.Equals(item.Id, this.Id);
        }

        public static bool operator ==(Entity<TId> left, Entity<TId> right)
        {
            if (left is null && right is null)
                return true;
            if (left is null || right is null)
                return false;

            return left.Equals(right);
        }

        public static bool operator !=(Entity<TId> left, Entity<TId> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = this.Id.GetHashCode() ^ 31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)

                return _requestedHashCode.Value;
            }
            else
                return base.GetHashCode();
        }
    }
}
