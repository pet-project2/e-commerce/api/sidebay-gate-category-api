﻿namespace Domain.Shared.Entities
{
    public class EntityException : Exception
    {
        public EntityException(string message) : base(message) 
        {
            
        }
    }
}
