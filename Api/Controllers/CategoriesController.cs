﻿using Application.Commands;
using Application.Commands.Update;
using Application.Dtos;
using Application.Queries.Details;
using Application.Queries.Lists;
using Domain.Shared.Dtos.Apis;
using Domain.Shared.Dtos.Resuls;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoriesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ApiQueryResponse<ListCategoryDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCategoriesAsync()
        {
            var listQuery = new ListCategoryQuery(1, 25);
            var result = await _mediator.Send(listQuery);
            if(!result.IsSuccess)
            {
                return StatusCode(
                   StatusCodes.Status500InternalServerError,
                   new ApiErrorResponse<string>
                   (
                       "Something happend when get list category",
                       StatusCodes.Status500InternalServerError,
                       ErrorType.INTERNAL_SERVER_ERROR,
                       result.Message
                   ));
            }

            return Ok(new ApiQueryResponse<ListCategoryDto>(
                                                result.Message,
                                                StatusCodes.Status200OK,
                                                result.Data));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ApiQueryResponse<CategoryDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCategory([FromRoute]Guid id)
        {
            var request = new CategoryQuery(id);
            var result = await _mediator.Send(request);
            if (!result.IsSuccess)
            {
                return StatusCode(
                   StatusCodes.Status500InternalServerError,
                   new ApiErrorResponse<string>
                   (
                       "Something happend when get category",
                       StatusCodes.Status500InternalServerError,
                       ErrorType.INTERNAL_SERVER_ERROR,
                       result.Message
                   ));
            }

            return Ok(new ApiQueryResponse<CategoryDto>(
                                                result.Message,
                                                StatusCodes.Status200OK,
                                                result.Data));
        }

        [HttpPost]
        [ProducesResponseType(typeof(ApiCommandResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateCategoryAsync([FromBody] CreateCategoryCommand command)
        {
            var result = await _mediator.Send(command);
            if (!result.IsSuccess)
            {
                return StatusCode(
                   StatusCodes.Status500InternalServerError,
                   new ApiErrorResponse<string>
                   (
                       "Something happend when create new category",
                       StatusCodes.Status500InternalServerError,
                       ErrorType.INTERNAL_SERVER_ERROR,
                       result.Message
                   ));
            }

            return StatusCode(
                    StatusCodes.Status200OK,
                    new ApiCommandResponse(result.Message, StatusCodes.Status200OK));
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ApiCommandResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCategoryAsync([FromRoute]Guid id, [FromBody]UpdateCategoryCommand command)
        {
            command.Id = id;
            var result = await _mediator.Send(command);
            if (!result.IsSuccess)
            {
                return StatusCode(
                   StatusCodes.Status500InternalServerError,
                   new ApiErrorResponse<string>
                   (
                       "Something happend when update category",
                       StatusCodes.Status500InternalServerError,
                       ErrorType.INTERNAL_SERVER_ERROR,
                       result.Message
                   ));
            }

            return StatusCode(
                    StatusCodes.Status200OK,
                    new ApiCommandResponse(result.Message, StatusCodes.Status200OK));
        }
    }
}
