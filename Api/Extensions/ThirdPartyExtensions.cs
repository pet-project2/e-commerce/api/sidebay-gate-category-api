﻿using Application.Mapper;
using Infrastructure.Mapper;
using System.Reflection;

namespace Api.Extensions
{
    public static class ThirdPartyExtensions
    {
        public static void RegistryThirdParty(this IServiceCollection services)
        {
            var assemblies = new[]
            {
                typeof(ApplicationMapper).GetTypeInfo().Assembly,
                typeof(InfrastructureMapper).GetTypeInfo().Assembly
            };

            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(assemblies));
            services.AddAutoMapper(assemblies);
        }
    }
}
