﻿using Infrastructure.Models;
using Infrastructure.Repositories;
using Infrastructure.Repositories.Interceptor;
using Infrastructure.Repositories.UnitOfWorks;

namespace Api.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterService(this IServiceCollection services)
        {
            services.AddScoped<ICategoryRepository<Category>, CategoryRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<SaveEntitiesInterceptor>();
        }
    }
}
