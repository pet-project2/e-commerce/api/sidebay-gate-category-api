using Api.Extensions;
using Infrastructure.Data;
using Infrastructure.Models;
using Infrastructure.Repositories.Interceptor;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

namespace Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var service = builder.Services;
            var configuration = builder.Configuration;

            ConfigureServices(service, configuration);

            // Configure the HTTP request pipeline
            var app = builder.Build();
            ConfigureWebApplication(app);
        }

        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.AddControllers();

            services.AddMigration<AppDbContext, SeedData>();
            services.RegisterService();
            services.RegistryThirdParty();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SideBay.Gate.Service.Category.Api", Version = "v1" });
            });

            // Database
            var dbConnection = configuration.GetConnectionString("SideBayGate");
            services.AddDbContext<AppDbContext>((service, options) =>
            {
                options.UseSqlServer(dbConnection)
                       .AddInterceptors(service.GetRequiredService<SaveEntitiesInterceptor>());
            });
        }

        private static void ConfigureWebApplication(WebApplication app)
        {
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SideBay.Gate.Service.Category.Api v1"));
            }

            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.Run();
        }
    }
}
