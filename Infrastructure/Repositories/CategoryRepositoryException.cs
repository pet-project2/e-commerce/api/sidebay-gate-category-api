﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class CategoryRepositoryException : Exception
    {
        public CategoryRepositoryException(string message) : base(message)
        {
            
        }
    }
}
