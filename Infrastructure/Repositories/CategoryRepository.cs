﻿using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class CategoryRepository : ICategoryRepository<Category>
    {
        private readonly AppDbContext _appDbContext;
        public CategoryRepository(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public async Task<Category> GetByIdAsync(Guid id)
        {
            var result = await this._appDbContext.Category
                                   .AsNoTracking()
                                   .Where(x => x.Id == id)
                                   .FirstOrDefaultAsync();
            return result;
        }

        public async Task<Category> GetByIdTrackingAsync(Guid id)
        {
            var result = await this._appDbContext.Category
                                   .Where(x => x.Id == id)
                                   .FirstOrDefaultAsync();
            return result;
        }

        public async Task<IEnumerable<Category>> GetListAsync()
        {
            var result = await this._appDbContext.Category.
                               AsNoTracking()
                               .Where(x => x.IsDeleted == false)
                               .ToListAsync();
            return result;
        }

        public Task CreateAsync(Category category)
        {
            this._appDbContext.Entry(category).State = EntityState.Added;
            return Task.CompletedTask;
        }

        public Task UpdateAsync(Category existCategory, Category newCategory)
        {
            this._appDbContext.Entry(existCategory).CurrentValues.SetValues(newCategory);
            this._appDbContext.Entry(existCategory).State = EntityState.Modified;
            return Task.CompletedTask;
        }

        public Task UpdateAsync(Category category)
        {
            this._appDbContext.Entry(category).State = EntityState.Modified;
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Category category)
        {
            this._appDbContext.Entry(category).State = EntityState.Deleted;
            return Task.CompletedTask;
        }

        public Task<int> GetCountAsync()
        {
            return this._appDbContext.Category.CountAsync();
        }
    }
}
