﻿using Domain.Shared.Models;
using Infrastructure.Models;

namespace Infrastructure.Repositories.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;

        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public Task Commit()

        {
            return this._appDbContext.SaveChangesAsync();
        }
    }
}
