﻿namespace Infrastructure.Repositories.UnitOfWorks
{
    public interface IUnitOfWork
    {
        Task Commit();
    }
}
