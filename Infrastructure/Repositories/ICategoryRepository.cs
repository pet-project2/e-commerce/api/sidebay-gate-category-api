﻿using Domain.Shared.Interface;
using Infrastructure.Models;

namespace Infrastructure.Repositories
{
    public interface ICategoryRepository<T> where T : class, IAggregateRoot
    {
        Task<IEnumerable<Category>> GetListAsync();
        Task<int> GetCountAsync();

        Task<Category> GetByIdAsync(Guid id);
        Task<Category> GetByIdTrackingAsync(Guid id);
        Task CreateAsync(Category category);
        Task UpdateAsync(Category existCategory, Category newCategory);
        Task UpdateAsync(Category category);
        Task DeleteAsync(Category category);
    }
}
