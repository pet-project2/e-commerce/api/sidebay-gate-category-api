﻿using AutoMapper;
using Domain.Aggregates;
using Domain.Aggregates.Entities;
using Infrastructure.Models;

namespace Infrastructure.Mapper
{
    public class CategoryMapper : Profile
    {
        public CategoryMapper()
        {
            CreateMap<CategoryAggregate, Category>()
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Description, s => s.MapFrom(src => src.Description))
                .ForMember(d => d.ParentCateId, s => s.MapFrom(src => src.ParentCateId))
                .ForMember(d => d.Level, s => s.MapFrom(src => src.Level))
                .ForMember(d => d.Gallery, s => s.MapFrom(src => src.Gallery))
                .ForMember(d => d.DomainEvents, s => s.MapFrom((src, des, result) =>
                {
                    result = src.DomainEvents;
                    src.ClearDomainEvents();
                    return result;
                }));

            CreateMap<Category, CategoryAggregate>()
                .ConstructUsing((source, destination) =>
                {
                    var categoryAgg = new CategoryAggregate(
                        source.Id,
                        source.Name
                    );

                    categoryAgg.SetLevel(source.Level);
                    categoryAgg.SetParentCategory(source.ParentCateId);

                    var images = InfrastructureMapper.Mapper.Map<List<Domain.Aggregates.Entities.Image>>(source.Gallery);
                    categoryAgg.SetGallery(images);
                    return categoryAgg;
                })
                .ForAllMembers(s => s.Ignore());

        }
    }
}
