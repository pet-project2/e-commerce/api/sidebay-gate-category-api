﻿using AutoMapper;
using Domain.Aggregates.Entities;

namespace Infrastructure.Mapper
{
    internal class ImageMapper : Profile
    {
        public ImageMapper()
        {
            CreateMap<Image, Models.Image>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Path, s => s.MapFrom(src => src.Path))
                .ForMember(d => d.IsMainAvatar, s => s.MapFrom(src => src.IsMainAvatar));
        }
    }
}
