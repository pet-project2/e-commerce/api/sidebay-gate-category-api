﻿using Domain.Shared.Models;

namespace Infrastructure.Models
{
    public class Image : BaseModel<Guid>
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsMainAvatar { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
