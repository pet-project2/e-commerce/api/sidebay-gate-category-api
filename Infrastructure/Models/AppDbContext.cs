﻿using Infrastructure.Configs;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ImageConfiguration());
        }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Image> Images { get; set; }
    }
}
