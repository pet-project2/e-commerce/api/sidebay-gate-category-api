﻿using Domain.Shared.Interface;
using Domain.Shared.Models;
using MediatR;

namespace Infrastructure.Models
{
    public class Category : BaseModel<Guid>, IAggregateRoot
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ParentCateId { get; set; }
        public int Level { get; set; }
        public ICollection<Image> Gallery { get; set; }
        public IReadOnlyCollection<INotification>? DomainEvents { get; set; }

        public void SetValue(Category category)
        {
            if (category == null)
                return;
            this.Name = category.Name;
            this.Description = category.Description;
            this.ParentCateId = category.ParentCateId;
            this.Level = category.Level;
            this.Gallery = category.Gallery;
        }
    }
}
