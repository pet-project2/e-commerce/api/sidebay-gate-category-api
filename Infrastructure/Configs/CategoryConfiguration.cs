﻿using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configs
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name);
            builder.Property(x => x.Description)
                    .IsRequired(false);
            builder.Property(x => x.ParentCateId);
            builder.Property(x => x.Level);
            builder.Property(x => x.IsDeleted);
            builder.Property(x => x.CreatedDateTime);
            builder.Property(x => x.LastModifiedDateTime);

            builder.Ignore(x => x.DomainEvents);

            builder.HasMany(x => x.Gallery)
               .WithOne(x => x.Category)
               .HasForeignKey(x => x.CategoryId)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
