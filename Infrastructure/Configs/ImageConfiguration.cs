﻿using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configs
{
    public class ImageConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder.ToTable("Images");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).IsRequired(false);
            builder.Property(x => x.Path);
            builder.Property(x => x.IsMainAvatar);
            builder.Property(x => x.IsDeleted);
            builder.Property(x => x.CreatedDateTime);
            builder.Ignore(x => x.LastModifiedDateTime);
        }
    }
}
