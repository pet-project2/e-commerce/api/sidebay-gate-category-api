﻿using Infrastructure.Models;

namespace Infrastructure.Data
{
    public class SeedData : IDbSeeder<AppDbContext>
    {
        public Task SeedAsync(AppDbContext context)
        {
            return Task.CompletedTask;
        }
    }
}
