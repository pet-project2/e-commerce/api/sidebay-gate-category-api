﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public interface IDbSeeder<in TContext> where TContext : DbContext
    {
        Task SeedAsync(TContext context);
    }
}
