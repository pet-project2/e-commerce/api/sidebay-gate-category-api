﻿using Application.Dtos;
using Domain.Shared.Dtos.Resuls;
using MediatR;

namespace Application.Queries.Details
{
    public class CategoryQuery : IRequest<IResultData<CategoryDto>>
    {
        public CategoryQuery(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; private set; }
    }
}
