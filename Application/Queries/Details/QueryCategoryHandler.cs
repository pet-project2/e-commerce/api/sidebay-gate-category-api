﻿using Application.Dtos;
using Application.Mapper;
using Domain.Shared.Dtos.Resuls;
using Infrastructure.Models;
using Infrastructure.Repositories;
using MediatR;

namespace Application.Queries.Details
{
    public class QueryCategoryHandler : IRequestHandler<CategoryQuery, IResultData<CategoryDto>>
    {
        private readonly ICategoryRepository<Category> _categoryRepository;
        public QueryCategoryHandler(ICategoryRepository<Category> categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }

        public async Task<IResultData<CategoryDto>> Handle(CategoryQuery request, CancellationToken cancellationToken)
        {
            var categoryId = request.Id;
            var category = await this._categoryRepository.GetByIdAsync(categoryId);

            var categoryDto = ApplicationMapper.Mapper.Map<CategoryDto>(category);
            return ResultData<CategoryDto>.SuccessData("Get Category Sucessfully", categoryDto);
        }
    }
}
