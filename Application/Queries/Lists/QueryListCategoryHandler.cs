﻿using Application.Dtos;
using Application.Mapper;
using Domain.Shared.Dtos.Resuls;
using Infrastructure.Models;
using Infrastructure.Repositories;
using MediatR;

namespace Application.Queries.Lists
{
    public class QueryListCategoryHandler : IRequestHandler<ListCategoryQuery, IResultData<ListCategoryDto>>
    {
        private readonly ICategoryRepository<Category> _categoryRepository;

        public QueryListCategoryHandler(ICategoryRepository<Category> categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }

        public async Task<IResultData<ListCategoryDto>> Handle(ListCategoryQuery request, CancellationToken cancellationToken)
        {
            var categories = await _categoryRepository.GetListAsync();
            var totalRows = await _categoryRepository.GetCountAsync();

            var categoryDtos = ApplicationMapper.Mapper.Map<IEnumerable<CategoryDto>>(categories);
            var listCategoryDto = new ListCategoryDto
            {
                Categories = categoryDtos,
                Paging = Paging.GetPaging(request.PageNumber, request.PageSize, totalRows)
            };

            var result = ResultData<ListCategoryDto>.SuccessData("Query list category successfully", listCategoryDto);
            return result;
        }
    }
}
