﻿using Application.Dtos;
using Domain.Shared.Dtos.Resuls;
using MediatR;

namespace Application.Queries.Lists
{
    public class ListCategoryQuery : IRequest<IResultData<ListCategoryDto>>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public ListCategoryQuery(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }
    }
}
