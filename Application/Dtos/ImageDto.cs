﻿namespace Application.Dtos
{
    public class ImageDto
    {
        public Guid Id { get; set; }
        public string Path { get; private set; }
    }
}
