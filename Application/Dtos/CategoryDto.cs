﻿namespace Application.Dtos
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ParentCateId { get; set; }
        public int Level { get; set; }
        public IReadOnlyCollection<ImageDto> Gallery { get; set; }
    }
}
