﻿using Domain.Shared.Dtos.Resuls;

namespace Application.Dtos
{
    public class ListCategoryDto  
    {
        public IEnumerable<CategoryDto> Categories { get; set; }
        public Paging Paging { get; set; }

    }
}
