﻿using Application.Dtos;
using AutoMapper;
using Infrastructure.Models;

namespace Application.Mapper
{
    public class ImageDtoMapper : Profile
    {
        public ImageDtoMapper()
        {
            CreateMap<Image, ImageDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Path, s => s.MapFrom(src => src.Path));
        }
    }
}
