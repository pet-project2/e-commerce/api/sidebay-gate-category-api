﻿using Application.Dtos;
using AutoMapper;
using Domain.Aggregates;
using Infrastructure.Models;

namespace Application.Mapper
{
    public class CategoryDtoMapper : Profile
    {
        public CategoryDtoMapper()
        {
            CreateMap<CategoryAggregate, CategoryDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Description, s => s.MapFrom(src => src.Description))
                .ForMember(d => d.ParentCateId, s => s.MapFrom(src => src.ParentCateId))
                .ForMember(d => d.Level, s => s.MapFrom(src => src.Level))
                .ForMember(d => d.Gallery, s => s.MapFrom(
                    src => ApplicationMapper.Mapper.Map<IEnumerable<ImageDto>>(src.Gallery)));

            CreateMap<Category, CategoryDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.ParentCateId, s => s.MapFrom(src => src.ParentCateId))
                .ForMember(d => d.Level, s => s.MapFrom(src => src.Level))
                .ForMember(d => d.Gallery, s => s.MapFrom(
                    src => ApplicationMapper.Mapper.Map<IEnumerable<ImageDto>>(src.Gallery)));
        }
    }
}
