﻿using Application.Commands;
using AutoMapper;
using Domain.Aggregates;
using Domain.Aggregates.Entities;
using Infrastructure.Mapper;

namespace Application.Mapper
{
    public class CreateCategoryCommandMapper : Profile
    {
        public CreateCategoryCommandMapper()
        {
            CreateMap<CreateCategoryCommand, CategoryAggregate>()
                .ConstructUsing((source, destination) =>
                {
                    var categoryAgg = new CategoryAggregate(
                        source.Name
                    );

                    var parentId = source.ParentCateId.HasValue ? source.ParentCateId.Value : default;
                    categoryAgg.SetCategoryInfo(source.Description, source.Level, parentId);

                    var images = InfrastructureMapper.Mapper.Map<List<Image>>(source.Gallery);
                    categoryAgg.SetGallery(images);
                    return categoryAgg;
                })
                .ForAllMembers(s => s.Ignore());
        }
    }
}
