﻿using AutoMapper;

namespace Application.Mapper
{
    public class ApplicationMapper
    {
        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CreateCategoryCommandMapper>();
                cfg.AddProfile<CategoryDtoMapper>();
                cfg.AddProfile<UpdateCategoryCommandMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => _lazy.Value;
    }
}
