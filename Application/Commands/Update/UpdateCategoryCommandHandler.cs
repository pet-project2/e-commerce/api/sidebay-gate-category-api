﻿using Application.Mapper;
using Domain.Aggregates;
using Domain.Shared.Dtos.Resuls;
using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Repositories;
using Infrastructure.Repositories.UnitOfWorks;
using MediatR;

namespace Application.Commands.Update
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, IResult>
    {
        private readonly ICategoryRepository<Category> _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        public UpdateCategoryCommandHandler(ICategoryRepository<Category> categoryRepository, IUnitOfWork unitOfWork)
        {
            this._categoryRepository = categoryRepository;
            this._unitOfWork = unitOfWork;
        }

        public async Task<IResult> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var cateId = request.Id;
            var existCategory = await _categoryRepository.GetByIdTrackingAsync(cateId);
            if (existCategory == null)
            {
                return Result.Failed("No category is existed for updating");
            }

            var cateDomain = ApplicationMapper.Mapper.Map<CategoryAggregate>(request);
            cateDomain.SetCategoryInfo(request.Description, request.Level, request.ParentCateId.HasValue ? request.ParentCateId.Value : default);
            // TODO Logic

            var category = InfrastructureMapper.Mapper.Map<Category>(cateDomain);
            

            existCategory.SetValue(category);
            await _categoryRepository.UpdateAsync(existCategory);
            await _unitOfWork.Commit();

            return Result.Success($"Update category successfully");
        }
    }
}
