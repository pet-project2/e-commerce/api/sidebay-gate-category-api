﻿using Domain.Shared.Dtos.Resuls;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Update
{
    public class UpdateCategoryCommand : IRequest<IResult>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ParentCateId { get; set; }
        public int Level { get; set; }
        public IEnumerable<string> Gallery { get; set; }
    }
}
