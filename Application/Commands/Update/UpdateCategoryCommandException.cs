﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Update
{
    public class UpdateCategoryCommandException : Exception
    {
        public UpdateCategoryCommandException(string message) : base(message)
        {
            
        }
    }
}
