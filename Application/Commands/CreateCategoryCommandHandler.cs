﻿using Application.Mapper;
using Domain.Aggregates;
using Domain.Shared.Dtos.Resuls;
using Infrastructure.Mapper;
using Infrastructure.Models;
using Infrastructure.Repositories;
using Infrastructure.Repositories.UnitOfWorks;
using MediatR;

namespace Application.Commands
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, IResult>
    {
        private readonly ICategoryRepository<Category> _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        public CreateCategoryCommandHandler(ICategoryRepository<Category> categoryRepository, IUnitOfWork unitOfWork)
        {
            this._categoryRepository = categoryRepository;
            this._unitOfWork = unitOfWork;
        }

        public async Task<IResult> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            var categoryAgg = ApplicationMapper.Mapper.Map<CategoryAggregate>(request);

            //
            var category = InfrastructureMapper.Mapper.Map<Category>(categoryAgg);
            await this._categoryRepository.CreateAsync(category);
            await this._unitOfWork.Commit();

            var result = Result.Success($"Create new product successfully");
            return result;
        }
    }
}
