﻿using Domain.Shared.Dtos.Resuls;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Application.Commands
{
    public class CreateCategoryCommand : IRequest<IResult>
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ParentCateId { get; set; }
        public int Level { get; set; }
        public IEnumerable<string>? Gallery { get; set; }
    }
}
